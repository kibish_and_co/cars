//
//  NSError+Ext.swift
//  Cars
//
//  Created by Nick Kibish on 12/23/16.
//  Copyright © 2016 Nick Kibish. All rights reserved.
//

import Foundation

public struct NSErrorKeys {
    static let httpStatusCode = "NSErrorKeys.httpStatusCode"
    static let errorDescription = "NSErrorKeys.errorDescription"
}

public extension NSError {
    var statusCode: Int? {
        return userInfo[NSErrorKeys.httpStatusCode] as? Int
    }
    
    var errorDescription: String? {
        return userInfo[NSErrorKeys.errorDescription] as? String
    }
}
