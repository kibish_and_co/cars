//
//  UIImage+Ext.swift
//  Cars
//
//  Created by Nick Kibish on 12/24/16.
//  Copyright © 2016 Nick Kibish. All rights reserved.
//

import UIKit

public enum TabBarIcon {
    case MapMarker
    case MapMarkerFilled
    case List
    case ListFilled
}

public extension UIImage {
    var data: Data? {
         return UIImagePNGRepresentation(self)
    }
    
    public class func tabBarImage(imageID: TabBarIcon) -> UIImage {
        switch imageID {
        case .MapMarker:
            return #imageLiteral(resourceName: "Map Marker")
        case .MapMarkerFilled:
            return #imageLiteral(resourceName: "Map Marker Filled")
        case .List:
            return #imageLiteral(resourceName: "List")
        case .ListFilled:
            return #imageLiteral(resourceName: "List Filled")
        }
    }
}
