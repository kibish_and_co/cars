//
//  UIViewController+Ext.swift
//  Cars
//
//  Created by Nick Kibish on 12/26/16.
//  Copyright © 2016 Nick Kibish. All rights reserved.
//

import UIKit

extension UIViewController {
    class func storyboardInstance() -> UIViewController? {
        let storyboardName = String(describing: self)
        if Bundle.main.path(forResource: storyboardName, ofType: "storyboardc") == nil {
            return nil 
        }
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard.instantiateInitialViewController()
    }
    
    func showAlert(title: String?, message: String?, buttonTitle: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: (buttonTitle != nil ? buttonTitle : "OK"), style: .cancel, handler: nil)
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }
}
