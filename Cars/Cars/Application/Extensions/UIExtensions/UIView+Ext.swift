//
//  UIView+Ext.swift
//  Cars
//
//  Created by Nick Kibish on 12/25/16.
//  Copyright © 2016 Nick Kibish. All rights reserved.
//

import UIKit

public extension UIView {
    static func nibInstance() -> UIView? {
        let nib = Bundle.main.loadNibNamed(String(describing: self), owner: nil, options: nil)
        return nib?.first as? UIView
    }
}
