//
//  UITableViewCell+Ext.swift
//  Cars
//
//  Created by Nick Kibish on 12/24/16.
//  Copyright © 2016 Nick Kibish. All rights reserved.
//

import UIKit

public extension UITableViewCell {
    static var cellID: String {
        return String(describing: self)
    }
}
