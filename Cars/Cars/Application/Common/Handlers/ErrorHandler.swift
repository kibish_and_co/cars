//
//  ErrorHandler.swift
//  Cars
//
//  Created by Nick Kibish on 12/23/16.
//  Copyright © 2016 Nick Kibish. All rights reserved.
//

import Foundation
import Alamofire

public struct DataFailureResponse {
    public let request: URLRequest?
    public let response: HTTPURLResponse?
    public let data: Data?
    public let timeline: Timeline
    
    init(response: DataResponse<[Any]>) {
        request = response.request
        self.response = response.response
        data = response.data
        timeline = response.timeline
    }
}

public class ErrorHandler {
    class func handle<T>(error: NSError, response: DataResponse<T>) -> NSError {
        var statusCode = -1
        if let serverStatusCode = response.response?.statusCode {
            statusCode = serverStatusCode
        }
        
        var errorDescription = error.description
        if let serverData = response.data, let serverMessage = String(data: serverData, encoding: .utf8) {
            errorDescription = serverMessage
        }
        
        return handle(error: error, httpStatusCode: statusCode, errorDescription: errorDescription)
    }
    
    class func handle(error: NSError, httpStatusCode: Int, errorDescription: String) -> NSError {
        var userInfo = error.userInfo
        userInfo[NSErrorKeys.httpStatusCode] = httpStatusCode
        userInfo[NSErrorKeys.errorDescription] = errorDescription
        
        let handledError = NSError(domain: error.domain, code: error.code, userInfo: userInfo)
        return handledError
    }
}
