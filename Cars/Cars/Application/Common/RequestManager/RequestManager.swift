//
//  RequestManager.swift
//  Cars
//
//  Created by Nick Kibish on 12/23/16.
//  Copyright © 2016 Nick Kibish. All rights reserved.
//

import UIKit
import Alamofire
import UnboxedAlamofire
import Unbox

public typealias APISuccessArray<T> = ([T]) -> ()
public typealias APIError = (NSError) -> ()

public class RequestManager {
    static let baseURL = "http://www.codetalk.de/"
    static let sharedManager = RequestManager()
    
    func makeRequest(fullURL: String, method: HTTPMethod) -> DataRequest {
        return Alamofire.request(fullURL, method: method).validate(statusCode: 200..<300)
    }
    
    func makeRequest(urlPath: String, method: HTTPMethod) -> DataRequest {
        let fullURL = RequestManager.baseURL + urlPath
        return makeRequest(fullURL: fullURL, method: method)
    }
    
    func makeGetRequest(urlPath: String) -> DataRequest {
        return makeRequest(urlPath: urlPath, method: .get)
    }
}

extension RequestManager {
    func createNetworkError() -> NSError {
        let userInfo = [NSErrorKeys.errorDescription: "Lost internet connection"]
        let error = NSError(domain: "NK.Cars", code: -1, userInfo: userInfo)
        return error
    }
}

//MARK: - API Extension
public extension RequestManager {
    func getCars(success: @escaping APISuccessArray<Car>, failure: @escaping APIError) {
        if !NetworkReachability.isConnectedToNetwork() {
            failure(createNetworkError())
            return 
        }
        makeGetRequest(urlPath: Car.routerPath).responseArray { (response: DataResponse<[Car]>) in
            switch response.result {
            case .success(let value):
                success(value)
                break
            case .failure(let error):
                failure(ErrorHandler.handle(error: (error as NSError), response: response))
                break
            }
        }
    }
}
