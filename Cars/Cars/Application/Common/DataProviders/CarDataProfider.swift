//
//  CarDataProfider.swift
//  Cars
//
//  Created by Nick Kibish on 12/25/16.
//  Copyright © 2016 Nick Kibish. All rights reserved.
//

import Foundation

class CarDataProfider {
    public var cars: [Car] = []
    
    public func reloadData(success: @escaping APISuccessArray<Car>, failure: @escaping APIError) {
        
        RequestManager.sharedManager.getCars(success: { [weak self] (cars) in
            self?.cars = cars
            success(cars)
        }, failure: failure)
    }
}
