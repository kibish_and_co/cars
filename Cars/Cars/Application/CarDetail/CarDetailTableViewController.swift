//
//  CarDetailTableViewController.swift
//  Cars
//
//  Created by Nick Kibish on 12/26/16.
//  Copyright © 2016 Nick Kibish. All rights reserved.
//

import UIKit
import SDWebImage

class CarDetailTableViewController: UITableViewController {
    @IBOutlet var makeLabel: UILabel!
    @IBOutlet var modelLabel: UILabel!
    @IBOutlet var driverLabel: UILabel!
    @IBOutlet var colorLabel: UILabel!
    @IBOutlet var fuelLevelLabel: UILabel!
    @IBOutlet var fuelTypeLabel: UILabel!
    @IBOutlet var transmissionLabel: UILabel!
    @IBOutlet var carImageView: UIImageView!
    
    public var car: Car!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        makeLabel.text = car.make
        modelLabel.text = car.modelName
        driverLabel.text = car.name
        colorLabel.text = car.color
        fuelLevelLabel.text = "\(car.fuelLevel)"
        fuelTypeLabel.text = car.fuelType
        transmissionLabel.text = car.transmission
        
        carImageView.sd_setImage(with: car.imageURL, placeholderImage: #imageLiteral(resourceName: "car_template"))
    }
}
