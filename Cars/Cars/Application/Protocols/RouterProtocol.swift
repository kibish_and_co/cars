//
//  RouterProtocol.swift
//  Cars
//
//  Created by Nick Kibish on 12/23/16.
//  Copyright © 2016 Nick Kibish. All rights reserved.
//

import Foundation
import Unbox

protocol RouterObject {
    static var routerPath: String { get }
}
