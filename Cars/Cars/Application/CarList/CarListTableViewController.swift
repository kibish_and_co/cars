//
//  CarListTableViewController.swift
//  Cars
//
//  Created by Nick Kibish on 12/24/16.
//  Copyright © 2016 Nick Kibish. All rights reserved.
//

import UIKit
import SVProgressHUD

class CarListTableViewController: UITableViewController {
    var dataProvider = CarListDataProfider()
    var headerView = CarListSortView.nibInstance() as? CarListSortView
    var rControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView?.sortDelegate = self
        
        reloadData()
        
        rControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        rControl.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        tableView.addSubview(rControl)
    }
    
    func reloadData() {
        SVProgressHUD.show()
        dataProvider.reloadData(success: { [weak self] (cars) in
            SVProgressHUD.dismiss()
            self?.tableView.reloadData()
            self?.rControl.endRefreshing()
        }) { [weak self] (error) in
            self?.rControl.endRefreshing()
            self?.tableView.reloadData()
            self?.showAlert(title: "Error", message: error.errorDescription, buttonTitle: "Cancel")
            SVProgressHUD.dismiss()
        }
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataProvider.cars.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CarListTableViewCell.cellID, for: indexPath) as? CarListTableViewCell else {
            return UITableViewCell()
        }
        
        cell.car = dataProvider.cars[indexPath.row]

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 32   
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let carDetailViewController = CarDetailTableViewController.storyboardInstance() as? CarDetailTableViewController else {
            return
        }
        
        let car = dataProvider.cars[indexPath.row]
        carDetailViewController.car = car
        navigationController?.pushViewController(carDetailViewController, animated: true)
    }
}

extension CarListTableViewController: CarListSortDelegate {
    func changedSortDescriptor(sortDescriptor: @escaping (Car, Car) -> Bool) {
        dataProvider.sortDescriptor = sortDescriptor
        dataProvider.updateData()
        tableView.reloadData()
    }
}
