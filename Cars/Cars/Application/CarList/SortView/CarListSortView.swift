//
//  CarListSortView.swift
//  Cars
//
//  Created by Nick Kibish on 12/25/16.
//  Copyright © 2016 Nick Kibish. All rights reserved.
//

import UIKit

protocol CarListSortDelegate {
    func changedSortDescriptor(sortDescriptor: @escaping SortCarLogic)
}

class CarListSortView: UIView {
    let sortTitles = ["Sort by driver", "Sort by car"]
    public var sortDelegate: CarListSortDelegate!
    
    @IBOutlet var sortButton: UIButton!
    
    @IBAction func changeSortType(_ sender: UIButton) {
        let tag = sortButton.tag >= (sortTitles.count - 1) ? 0 : sortButton.tag + 1
        sortButton.tag = tag
        sortButton.setTitle(sortTitles[tag], for: .normal)
        
        let sortDescriptors = [CarSortDescriptor.driver, CarSortDescriptor.model]
        sortDelegate.changedSortDescriptor(sortDescriptor: sortDescriptors[sortButton.tag])
    }
    
}
