//
//  CarListDataProfider.swift
//  Cars
//
//  Created by Nick Kibish on 12/25/16.
//  Copyright © 2016 Nick Kibish. All rights reserved.
//

import UIKit

typealias SortCarLogic = (Car, Car) -> Bool

struct CarSortDescriptor {
    static let driver: SortCarLogic = { $0.name < $1.name }
    static let model: SortCarLogic = {
        if $0.make == $1.make {
            return $0.modelName < $1.modelName
        } else {
            return $0.make < $1.make
        }
    }
}

class CarListDataProfider: CarDataProfider {
    var sortDescriptor: SortCarLogic = CarSortDescriptor.driver
    
    override func reloadData(success: @escaping ([Car]) -> (), failure: @escaping APIError) {
        super.reloadData(success: { [weak self] (cars) in
            self?.updateData()
            success(cars)
        }, failure: failure)
    }
    
    public func updateData() {
        self.cars.sort(by: sortDescriptor)
    }
}
