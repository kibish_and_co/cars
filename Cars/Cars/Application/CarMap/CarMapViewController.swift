//
//  CarMapViewController.swift
//  Cars
//
//  Created by Nick Kibish on 12/25/16.
//  Copyright © 2016 Nick Kibish. All rights reserved.
//

import UIKit
import MapKit
import SVProgressHUD

class CarMapViewController: UIViewController {
    @IBOutlet var mapView: MKMapView!
    var dataProvider = CarDataProfider()
    var currentMarkerIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.reloadData()
    }
    
    func setCentterPin(anotation: MKAnnotation) {
        let adjustRegion = mapView.regionThatFits(MKCoordinateRegionMakeWithDistance(anotation.coordinate, 500, 500))
        mapView.setRegion(adjustRegion, animated: true)
        mapView.selectAnnotation(anotation, animated: true)
    }
    
    func updateAnotations(cars: [Car]) {
        mapView.removeAnnotations(mapView.annotations)
        
        for car in cars {
            let anotation = MKPointAnnotation()
            let coordinate = CLLocationCoordinate2D(latitude: car.latitude, longitude: car.longitude)
            anotation.coordinate = coordinate
            anotation.title = "\(car.make) \(car.modelName)"
            anotation.subtitle = car.name
            mapView.addAnnotation(anotation)
        }
        
        guard let anotation = mapView.annotations.first else {
            return
        }
        setCentterPin(anotation: anotation)
    }
}

//MARK: Actions
extension CarMapViewController {
    @IBAction func switchMarker(sender: UIBarButtonItem) {
        currentMarkerIndex += sender.tag
        if currentMarkerIndex >= mapView.annotations.count {
            currentMarkerIndex = 0
        } else if currentMarkerIndex <= 0 {
            currentMarkerIndex = mapView.annotations.count - 1
        }
        
        if mapView.annotations.count == 0 {
            return
        }
        
        setCentterPin(anotation: mapView.annotations[currentMarkerIndex])
    }
    
    @IBAction func reloadData() {
        SVProgressHUD.show()
        dataProvider.reloadData(success: { [weak self] (cars) in
            SVProgressHUD.dismiss()
            self?.updateAnotations(cars: cars)
        }) { [weak self] (error) in
            SVProgressHUD.dismiss()
            self?.showAlert(title: "Error", message: error.errorDescription, buttonTitle: "Cancel")
        }
    }
}
