//
//  Car.swift
//  Cars
//
//  Created by Nick Kibish on 12/22/16.
//  Copyright © 2016 Nick Kibish. All rights reserved.
//

import Foundation
import Unbox

public enum CarError: Error {
    case invalidCarImageURL
}

public struct Car {
    var id: String
    var modelIdentifier: String
    var modelName: String
    var name: String
    var make: String
    var group: String
    var color: String
    var series: String
    var fuelType: String
    var fuelLevel: Double
    var transmission: String
    var licensePlate: String
    var latitude: Double
    var longitude: Double
    
    var imageURL: URL
}

extension Car: Unboxable {
    public init(unboxer: Unboxer) throws {
        self.id = try unboxer.unbox(key: "id")
        self.modelIdentifier = try unboxer.unbox(key: "modelIdentifier")
        self.modelName = try unboxer.unbox(key: "modelName")
        self.name = try unboxer.unbox(key: "name")
        self.make = try unboxer.unbox(key: "make")
        self.group = try unboxer.unbox(key: "group")
        self.color = try unboxer.unbox(key: "color")
        self.series = try unboxer.unbox(key: "series")
        self.fuelType = try unboxer.unbox(key: "fuelType")
        self.fuelLevel = try unboxer.unbox(key: "fuelLevel")
        self.transmission = try unboxer.unbox(key: "transmission")
        self.licensePlate = try unboxer.unbox(key: "licensePlate")
        self.latitude = try unboxer.unbox(key: "latitude")
        self.longitude = try unboxer.unbox(key: "longitude")
        
        guard let imageURL = URL(string: "https://prod.drive-now-content.com/fileadmin/user_upload_global/assets/cars/\(modelIdentifier)/\(color)/2x/car.png") else {
            throw CarError.invalidCarImageURL
        }
        self.imageURL = imageURL
    }
}

extension Car: RouterObject {
    static var routerPath: String {
        return "cars.json"
    }
}
