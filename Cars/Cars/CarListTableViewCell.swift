//
//  CarListTableViewCell.swift
//  Cars
//
//  Created by Nick Kibish on 12/24/16.
//  Copyright © 2016 Nick Kibish. All rights reserved.
//

import UIKit
import SDWebImage

public class CarListTableViewCell: UITableViewCell {
    @IBOutlet var carImageView: UIImageView!
    @IBOutlet var carNameLabel: UILabel!
    @IBOutlet var driverNameLabel: UILabel!
    
    var car: Car! {
        didSet {
            carImageView.sd_setImage(with: car.imageURL, placeholderImage: #imageLiteral(resourceName: "car_template"))
            carNameLabel.text = "\(car.make) \(car.modelName)"
            driverNameLabel.text = car.name
        }
    }
}
