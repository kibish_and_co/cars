//
//  UIImageExtensionTest.swift
//  Cars
//
//  Created by Nick Kibish on 12/24/16.
//  Copyright © 2016 Nick Kibish. All rights reserved.
//

import XCTest
@testable import Cars

class UIImageExtensionTest: XCTestCase {
    var tabBarIcons = [#imageLiteral(resourceName: "Map Marker"), #imageLiteral(resourceName: "Map Marker Filled"), #imageLiteral(resourceName: "List"), #imageLiteral(resourceName: "List Filled")]
    var tabBarIconsIDs: [TabBarIcon] = [.MapMarker, .MapMarkerFilled, .List, .ListFilled]
    
    func testImageData() {
        for image in tabBarIcons {
            XCTAssertNotNil(image.data)
        }
    }
    
    func testTabBarIcons() {
        for image in tabBarIcons {
            let index = tabBarIcons.index(of: image)
            XCTAssertEqual(UIImage.tabBarImage(imageID: tabBarIconsIDs[index!]).data, image.data)
        }
    }
}
