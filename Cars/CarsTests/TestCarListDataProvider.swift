//
//  TestCarListDataProvider.swift
//  Cars
//
//  Created by Nick Kibish on 12/26/16.
//  Copyright © 2016 Nick Kibish. All rights reserved.
//

import XCTest
@testable import Cars

class TestCarListDataProvider: XCTestCase {
    var dataProvider = CarListDataProfider()
    var car1 = Car(id: "car1id", modelIdentifier: "car1modelID", modelName: "b", name: "acar1Name", make: "b", group: "car1Group", color: "car1Color", series: "car1Series", fuelType: "car1FuelType", fuelLevel: 1.0, transmission: "car1transmission", licensePlate: "car1licensePlate", latitude: 0.1, longitude: 0.1, imageURL: URL(string: "https://prod.drive-now-content.com/fileadmin/user_upload_global/assets/cars/mini/midnight_black/2x/car.png")!)
    
    var car2 = Car(id: "car1id", modelIdentifier: "car1modelID", modelName: "a", name: "bcar1Name", make: "a", group: "car1Group", color: "car1Color", series: "car1Series", fuelType: "car1FuelType", fuelLevel: 1.0, transmission: "car1transmission", licensePlate: "car1licensePlate", latitude: 0.1, longitude: 0.1, imageURL: URL(string: "https://prod.drive-now-content.com/fileadmin/user_upload_global/assets/cars/mini/midnight_black/2x/car.png")!)
    
    override func setUp() {
        super.setUp()
        
        dataProvider.cars.append(car1)
        dataProvider.cars.append(car2)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testSorting() {
        dataProvider.sortDescriptor = CarSortDescriptor.model
        dataProvider.updateData()
        
        XCTAssertTrue(dataProvider.cars[0].name == "bcar1Name")
        XCTAssertTrue(dataProvider.cars[1].name == "acar1Name")
        
        
        dataProvider.sortDescriptor = CarSortDescriptor.driver
        dataProvider.updateData()
        
        XCTAssertTrue(dataProvider.cars[0].name == "acar1Name")
        XCTAssertTrue(dataProvider.cars[1].name == "bcar1Name")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
