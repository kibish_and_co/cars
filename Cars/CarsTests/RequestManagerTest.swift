//
//  RequestManagerTest.swift
//  Cars
//
//  Created by Nick Kibish on 12/23/16.
//  Copyright © 2016 Nick Kibish. All rights reserved.
//

import XCTest
@testable import Cars

class RequestManagerTest: XCTestCase {
    func testExecutingCars() {
        let asyncExpectation = expectation(description: "Expect cars")
        var testCars = [Car]()
        var err: NSError? = nil
        
        RequestManager.sharedManager.getCars(success: { (cars) in
            testCars = cars 
            asyncExpectation.fulfill()
        }) { (error) in
            err = error
            asyncExpectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 5) { (error) in
            XCTAssertNil(err, "Something went horribly wrong")
            XCTAssertGreaterThan(testCars.count, 0)
        }
    }
}
