//
//  ErrorHandlerTest.swift
//  Cars
//
//  Created by Nick Kibish on 12/23/16.
//  Copyright © 2016 Nick Kibish. All rights reserved.
//

import XCTest
@testable import Cars
import Alamofire

class ErrorHandlerTest: XCTestCase {
    func testErrorHandlerWithResponse() {
        let error = NSError(domain: "com.cars", code: 0, userInfo: nil)
        
        let serverMessage = "Page not found"
        let data = serverMessage.data(using: .utf8)
        let statusCode = 404
        
        let httpResponse = HTTPURLResponse(url: URL(string: RequestManager.baseURL)!, statusCode: statusCode, httpVersion: nil, headerFields: nil)
        
        let response = DataResponse<Any>(request: nil, response: httpResponse, data: data, result: Result.failure(error), timeline: Timeline())
        
        let handledError = ErrorHandler.handle(error: error, response: response)
        
        XCTAssertEqual(handledError.errorDescription, serverMessage)
        XCTAssertEqual(handledError.statusCode, statusCode)
    }
    
    func testErrorHandler() {
        let error = NSError(domain: "com.cars", code: 0, userInfo: nil)
        
        let errorMessage = "Page Not Found"
        let handledError = ErrorHandler.handle(error: error, httpStatusCode: 404, errorDescription: errorMessage)
        
        XCTAssertEqual(handledError.errorDescription, errorMessage)
        XCTAssertEqual(handledError.statusCode, 404)
    }
}
