# CARS Test Project #

### Installation ###

Run `pod install` to install all pods before run the project.

### Pods description ###
1. [Alamofire](https://github.com/Alamofire/Alamofire): Good lightweight library for working with networking.
2. [SDWebImage](https://github.com/rs/SDWebImage): Library for caching images
3. [Unbox](https://github.com/JohnSundell/Unbox): Really awesome library by John Sundell from Spotify. It's used for initializing model objects right from dictionary.
4. [UnboxedAlamofire](https://github.com/serejahh/UnboxedAlamofire): extension for Alamofire to make work with Unbox more useful.
5. [SDProgressHUD](https://github.com/SVProgressHUD/SVProgressHUD): nice loader view.

### Unit Tests ###
You can also run unit tests before running application.